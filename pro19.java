import java.util.Scanner;
public class Assignment19 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        // This pro accept a num and test whether its even or odd using condtional operator
        Scanner in = new Scanner (System.in);
        System.out.println("Enter an integer number: ");
        int num = in.nextInt();
        String result = (num % 2 == 0)? num + " is  an Even number": num + " is an Odd number";
        System.out.println(result);
        
    }
    
}
